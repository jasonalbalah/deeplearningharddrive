DEBUG := 
d = $$(date +%s)
start = `date +%s`

i = $(grep -c ^processor /proc/cpuinfo)
$(info $(i))
# vars.
export MAKEFLAGS="-j 8"
CXX = g++
CPP_FLAGS = -lrt -std=c++14 -Wall -pedantic -Wfloat-equal -g
EXEC = main
SOURCES := $(wildcard src/*.cpp)
OBJECTS = $(subst src/,,$(addprefix obj/, $(SOURCES:.cpp=.o)))
LIB = -Llib -g
INC = -Iinclude
#for headerDir in $(INC); do#
#	headerDir = -I$()
#done

ifdef DEBUG
$(info Sources: $(SOURCES))
$(info Objects: $(OBJECTS))
$(info Headers: $(INC))
$(info Libs: $(LIB))
endif

.PHONY: clean

all: $(EXEC) run

# main target
$(EXEC): $(OBJECTS)
	$(CXX) $(OBJECTS) -o $(EXEC)
 
# object files
obj/%.o: src/%.cpp
	$(CXX) $(LIB) $(INC) -c $(CPP_FLAGS) $< -o $@
 
# remove generated files
clean:
	rm -f obj/*.o
	rm -f obj/*/*.o
	rm -f main.exe
run:
	./main