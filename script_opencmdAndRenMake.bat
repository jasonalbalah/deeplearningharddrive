cd /d %~dp0
@echo off
rename Makefile.txt Makefile
del Makefile.txt
set "sessionSrc=C:/cygwin64/home/Jason Albalah/Session.vim"
doskey m=vim Makefile
doskey mk=make 
doskey u=cd ..
doskey p=pwd
doskey l=ls
doskey la=ls -a
doskey vm=vim -S "c:/cygwin/home/Jason Albalah/Session.vim"
doskey tm="C:\cygwin64\bin\time.exe" make
REM @echo on
cmd.exe /k "make"