#include <cassert>

#include "Random.h"

Random::Random()
: generator(SEED), dist_ur(0.0,1.0), dist_ui(0, 100)
{}

// create a new random range
double Random::rand(double a, double b)
{
    dist_ur = std::uniform_real_distribution<double>(a, b);
    return dist_ur(generator);
}

double Random::rand()
{
    return dist_ur(generator);
}

// create a new random range
int Random::randint(int a, int b)
{
    dist_ui = std::uniform_int_distribution<int>(a, b);
    return dist_ui(generator);
}

int Random::randint()
{
    return dist_ui(generator);
}

void test()
{
    Random r = Random();
    assert(r.randint(50,100) >= 50 && r.randint(50,100) <= 100);
    assert(r.randint() >= 50 && r.randint() <= 100);
}