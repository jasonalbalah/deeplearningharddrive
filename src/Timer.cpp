#include "Timer.h"
#include <iostream>

#define BILLION  1000000000L;

Timer::Timer()
{ }

void Timer::start()
{
    if(clock_gettime(CLOCK_REALTIME, &this->begin) == -1) {
      perror("clock gettime");
      exit(EXIT_FAILURE);
    }
}

Timer::UINT Timer::getElapsed(bool print=false)
{
    clock_gettime(CLOCK_REALTIME, &this->end);
    this->elapsed = (this->end.tv_nsec - this->begin.tv_nsec) / 1e9;
    if(print)
    {
        printf("Seconds: %lf\n", this->elapsed);
    }
    return this->elapsed;
}