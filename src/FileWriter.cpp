#include "FileWriter.h"

FileWriter::FileWriter(const char *path,
                       FileWriter::UINT size)
{  
    this->pFile = fopen(path, "wb");
    this->size = size;
    this->arr = new unsigned long long[size];
}

FileWriter::~FileWriter()
{  
    fclose(this->pFile);
    delete [] this->arr;
}

void FileWriter::write()
{
    for (unsigned long long j = 0; j < 1024; ++j){
        //Some calculations to fill a[]
        fwrite(this->arr, 
               1, 
               this->size * sizeof(unsigned long long), 
               this->pFile);
    }
}