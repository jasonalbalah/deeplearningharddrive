#ifndef RANDOM_H
#define RANDOM_H

#include <random>
#include <chrono>
#include <iostream>

class Random
{
public:
    Random();
    double rand(double, double);
    double rand();
    int randint(int, int);
    int randint();
    void test();
private:
    typedef std::chrono::system_clock CLOCK;
    unsigned int SEED = CLOCK::now().time_since_epoch().count();
    std::mt19937 generator;
    std::uniform_real_distribution<double> dist_ur;
    std::uniform_int_distribution<int> dist_ui;
};

#endif