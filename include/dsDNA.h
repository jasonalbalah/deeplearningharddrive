#ifndef DSDNA_H
#define DSDNA_H

#include <vector>

template <class T>
class dsDNA
{
public:
    dsDNA(std::vector<T>);

private:
    std::vector<T> seq5;
    std::vector<T> seq3;
};

#endif