#ifndef TIMER_H
#define TIMER_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

class Timer
{
public:
    Timer();
    void start();
    typedef unsigned long long UINT;
    UINT getElapsed(bool print);
private:
    struct timespec begin={0}, end={0};
    double elapsed=0.;
};

#endif