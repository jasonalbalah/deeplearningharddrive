#ifndef FILEWRITER_H
#define FILEWRITER_H

#include <stdio.h>

class FileWriter
{
public:
    typedef unsigned long long UINT;
    FileWriter(const char *path="file.binary",
               UINT size=8ULL*1024ULL*1024ULL);
    ~FileWriter();
    void write();
private:
    UINT size=0;
    UINT *arr = 0;
    FILE *pFile=0;
};

#endif